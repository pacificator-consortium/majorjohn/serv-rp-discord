import * as Assert from "assert";
import * as Discord from "discord.js";
import { __ } from 'i18n';

import TestHelper from "./testHelper";

import Global from "./../src/global";

import AbstractCmd from "./../src/commands/abstractCmd";
import PlayersCmd from "./../src/commands/player/playersCmd";
import PlayerDescriptionCmd from "./../src/commands/player/playerDescriptionCmd";
import PlayerStatCmd from "./../src/commands/player/playerStatCmd";
import PlayerCreateCmd from "./../src/commands/player/playerCreateCmd";

import PlayerControlServices from "./../src/control/playerControlServices";



describe("/player Cmd", function () {
	this.timeout(5000);

	before((done: MochaDone) => {
		TestHelper.setUpConfig();
		TestHelper.connectToBDD(done);
	});

	after((done: MochaDone) => {
		TestHelper.disconnectToBDD();
		done()
	});

	it("/players", (done: MochaDone) => {
		let playerControlService = new PlayerControlServices();
		let cmd = new PlayersCmd(playerControlService);

		let message: Discord.Message = new class extends Discord.Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<Discord.MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				return null;
			}
		};
		message.author = new class extends Discord.User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Discord.Message | Array<Discord.Message>)> {

				Assert.ok(content.startsWith(__("playerList")));
				done();
				return null;
			}
		};
		message.content = "/players";

		cmd.handle(message);
	});

	it("/playerCreate", (done: MochaDone) => {
		let playerControlService = new PlayerControlServices();
		let cmd = new PlayerCreateCmd(playerControlService);

		let message: Discord.Message = new class extends Discord.Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<Discord.MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				return null;
			}
		};
		message.author = new class extends Discord.User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Discord.Message | Array<Discord.Message>)> {

				Assert.ok(content.startsWith(__("playerCreateInfoStat")));
				done();
				return null;
			}
		};
		message.content = "/playerCreate mj superTest yeah";
		message.member = new class extends Discord.GuildMember {
			constructor() {
				super(new Discord.Guild(null, null), null);
			}
			setNickname(nick, reason): Promise<Discord.GuildMember> {
				Assert.equal(nick, "mj");
				return null;
			}
		};

		cmd.handle(message);
	});

	it("/playerStatInit", (done: MochaDone) => {
		let playerControlService = new class extends PlayerControlServices {
			setRole(message: Discord.Message, role: Discord.Role): void {

			}

			findRole(message: Discord.Message, roleName: string): Discord.Role {
				Assert.equal(roleName, "nux-345");
				return null;
			}
		}

		let cmd = new PlayerStatCmd(<PlayerControlServices>playerControlService);

		let message: Discord.Message = new class extends Discord.Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<Discord.MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				return null;
			}
		};
		message.author = new class extends Discord.User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Discord.Message | Array<Discord.Message>)> {

				Assert.ok(content.startsWith(__("playerNew", "mj")));
				done();
				return null;
			}
		};
		message.member = new class extends Discord.GuildMember {
			constructor() {
				super(new Discord.Guild(null, null), null);
			}
			addRole(role, reason): Promise<Discord.GuildMember> {
				return null;
			}
		};


		//message.guild.roles.find

		message.content = "/playerStatInit 2 2 2 2 2";

		cmd.handle(message);
	});

	it("/playerDescriptionGet", (done: MochaDone) => {
		let playerControlService = new PlayerControlServices();
		let cmd = new PlayerDescriptionCmd(playerControlService);
		let name: string = "mj";

		let message: Discord.Message = new class extends Discord.Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<Discord.MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				return null;
			}
		};
		message.author = new class extends Discord.User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Discord.Message | Array<Discord.Message>)> {
				Assert.ok(content.startsWith(name));
				done();
				return null;
			}
		};
		message.content = "/playerDescriptionGet " + name;

		cmd.handle(message);
	});
});