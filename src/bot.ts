import * as Discord from "discord.js";
import * as fs from 'fs';
import * as mongoose from 'mongoose';
import { configure as i18nConfigure, __ } from 'i18n';
import { configure, getLogger, Logger } from 'log4js';
import { join as Join } from "path";

import Lieu from "./commands/lieu";
import Global from './global';

//To Keep
import AbstractCmd from "./commands/abstractCmd";

import PlayersCmd from "./commands/player/playersCmd";
import PlayerDescriptionCmd from "./commands/player/playerDescriptionCmd";
import PlayerStatCmd from "./commands/player/playerStatCmd";
import PlayerCreateCmd from "./commands/player/playerCreateCmd";
import MoveCmd from "./commands/moveCmd";
import DefiCmd from "./commands/defiCmd";

import PlayerControlServices from "./control/playerControlServices";
import DiscordControlServices from "./control/discordControlServices";
import DefiControlServices from "./control/defiControlServices";

export class Bot extends Discord.Client {
	public static readonly GLOBAL_LOGGER: Logger = getLogger("global");

	public static readonly PROJECT_FOLDER_ROOT: string = Join(__dirname, "..");
	public static readonly CONFIG_FOLDER: string = Join(Bot.PROJECT_FOLDER_ROOT, "config");
	public static readonly LOGGER_CONFIG_PATH: string = Join(Bot.CONFIG_FOLDER, "log4js.json");
	public static readonly LOCAL_FOLDER: string = Join(Bot.PROJECT_FOLDER_ROOT, "locales");

	private static readonly MONGODB_URL: string = 'mongodb://mongo/servrp';
	private static readonly DROP_DB_ON_START: boolean = false;

	private localToken: string;
	private logger: Logger;

	private commands: AbstractCmd[];

	constructor() {
		super();
		this.localToken = fs.readFileSync('./config/token.cfg').toString();
		this.commands = [];
		this.setUpCommand();
		this.connectDB();
		Lieu.loadLieux();

		this.on("message", this.onMessage);
		Bot.GLOBAL_LOGGER.info("Bot start up.");
	}

	public start(): void {
		this.login(this.localToken);
	}

	private setUpCommand() {
		let playerControlService = new PlayerControlServices();
		let discordControlServices = new DiscordControlServices();
		let defiControlServices = new DefiControlServices();
		this.commands.push(new PlayersCmd(playerControlService));
		this.commands.push(new PlayerDescriptionCmd(playerControlService));
		this.commands.push(new PlayerStatCmd(playerControlService));
		this.commands.push(new PlayerCreateCmd(playerControlService));
		this.commands.push(new MoveCmd(discordControlServices));
		this.commands.push(new DefiCmd(playerControlService, defiControlServices));
	}

	private onMessage(message: Discord.Message): void {
		let chronoStart = new Date();

		if (!message.author.bot && message.channel.type == 'text') {
			if (message.content[0] === "/") {
				this.commands.some((cmd) => {
					if (cmd.verifyPrefix(message.content) && cmd.verifyCmdExist(message.content)) {
						if (cmd.verifyLocation((<Discord.TextChannel>message.channel).name)) {
							if (cmd.verifyArgValid(message.content)) {
								cmd.handle(message);
							} else {
								const cmdString: string = message.content.split(" ")[0].substring(1);
								Global.sendAndReact(message, __("cmdUse") + __(cmdString + "Use"), Global.warnTag);
							}
						} else {
							Global.sendAndReact(message, __("wrongChannel") + cmd.getLocation(), Global.warnTag);
						}
						return true; //Equivalent to Break
					}
				});
			} else {
				console.log("Checkout Place");
				//Lieu.handleLieu(message);
			}
			Bot.GLOBAL_LOGGER.info(`Temps Traitement message: ${((new Date()).getTime() - (chronoStart.getTime())) / 1000}s. 100 first Char: ${message.content.substr(0, 100)} `);
		}
	}

	private async connectDB(): Promise<void> {
		Bot.GLOBAL_LOGGER.info("Connect to database");
		try {
			await mongoose.connect(Bot.MONGODB_URL,
				{ useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true });
			if (Bot.DROP_DB_ON_START) {
				mongoose.connection.db.dropDatabase();
			}
		} catch (err) {
			Bot.GLOBAL_LOGGER.error("Connection error: " + err);
		}
	}
}

configure(Bot.LOGGER_CONFIG_PATH);
i18nConfigure({
	defaultLocale: 'fr',
	locales: ['fr'],
	directory: Bot.LOCAL_FOLDER
});

let bot = new Bot();
bot.start();