import { __ } from 'i18n';
import { Message } from "discord.js";

export default abstract class AbstractCmd {
	private prefix: string = "";
	private validCmds: string[] = [];
	protected locations: string[] = [];
	protected description: string = "";

	constructor(prefixParam: string, validCmdsParam: string[]) {
		this.prefix = prefixParam;
		this.validCmds = validCmdsParam;
	}

	public abstract handle(message: Message): void;

	public abstract verifyArgValid(message: string): boolean;

	protected getCmd(message: string) {
		return message.split(" ")[0];
	}

	public verifyCmdExist(message: string): boolean {
		return this.validCmds.indexOf(this.getCmd(message)) != -1;
	}

	public verifyPrefix(message: string): boolean {
		return message.startsWith(this.prefix);
	}

	/** Check if cmd must be execute in this channel */
	public verifyLocation(channelName: string): boolean {
		return this.locations.length == 0 || this.locations.indexOf(channelName) != -1;
	}

	/** Check if cmd have exactly expectedArg */
	protected checkArg(message: string, expectedArg: number) {
		return (message.split(" ").length - 1) === expectedArg;
	}

	/** Check if cmd have at last expectedArg */
	protected checkArgMax(message: string, expectedArg: number) {
		return (message.split(" ").length - 1) <= expectedArg;
	}

	/** Check if cmd have at least expectedArg */
	protected checkArgMin(message: string, expectedArg: number) {
		return (message.split(" ").length - 1) >= expectedArg;
	}

	public getPrefix(): string {
		return this.prefix;
	}

	public getValidCmds(): string[] {
		return this.validCmds;
	}

	public getLocation(): string {
		let list: string = "";
		for (let location of this.locations) {
			list += `${location} `;
		}
		return list;
	}
}
