import * as Assert from "assert";

import { connect, connection } from 'mongoose';
import { configure as i18nConfigure } from 'i18n';
import { configure } from 'log4js';

import Player from './../src/models/player';

export default class TestHelper {
	private constructor() { }

	public static connectToBDD(done: MochaDone): void {
		connect('mongodb://localhost/servrp-test', function (err) {
			if (err) {
				console.log(err)
			} else {
				Player.find({}).remove().exec(function (err, data) {
					done();
				});
			}
		});
	}

	public static setUpConfig(): void {
		configure('./src/config/log4js.json');
		i18nConfigure({
			defaultLocale: 'fr',
			locales: ['fr'],
			directory: __dirname + '/../src/locales'
		});
	}

	public static disconnectToBDD(): void {
		connection.close()
	}

	public static createFixture(done: Function) {
		console.log("createFixture");
		TestHelper.createPlayer("1", "att", "att", () => {
			TestHelper.createPlayer("2", "def", "def", done);
		});
	}

	public static deleteFixture(done: Function) {
		Player.find({}).remove().exec(function (err, data) { done() });
	}

	private static createPlayer(idParam: string, nameParam: string, descriptionParam: string, toExec: Function) {
		new Player({ idreal: idParam, name: nameParam, description: descriptionParam })
			.save((err) => {
				if (!err) {
					toExec();
				}
			});
	}
}
