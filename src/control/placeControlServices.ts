import Global from './../global';
import {error} from './../error';
import { __ } from 'i18n';

/*KeepIT*/

import {Query} from 'mongoose';
import {Message, TextChannel, Role} from 'discord.js';
import {Logger, getLogger} from 'log4js';

/*Models*/
import Lieu from './../models/lieu';

/*Control Services*/
import ErrorControlServices from "./errorControlServices";


export default class PlayerControlServices {
	private static logger: Logger = getLogger("error");

	/* Query: Get all places */
	public getPlacesQuery() {
		return Lieu.find({});
	}
}