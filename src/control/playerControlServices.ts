import Global from './../global';
import { error } from './../error';
import { __ } from 'i18n';

/*KeepIT*/

import { Query } from 'mongoose';
import { Message, TextChannel, Role } from 'discord.js';
import { Logger, getLogger } from 'log4js';

/*Models*/
import Player from './../models/player';

/*Control Services*/
import ErrorControlServices from "./errorControlServices";


export default class PlayerControlServices {
	private static logger: Logger = getLogger("error");

	/* Query: Get Player by intern Id */
	public getPlayerByIdQuery(id: string) {
		return Player.findOne({ idreal: id });
	}

	/* Query: Get Player by name */
	public getPlayerByNameQuery(name: string) {
		return Player.findOne({ name: name });
	}

	/* Query: Get all players */
	public getPlayersQuery() {
		return Player.find({});
	}

	public getPlayerDescription(message: Message, player) {
		if (player) {
			Global.sendAndReact(message,
				__("playerDescription", player.name, player.description,
					player.force, player.agilite, player.protection,
					player.esquive, player.endurance),
				Global.okTag);
		} else {
			Global.sendAndReact(message, __("playerUnknow"), Global.warnTag);
		}
	}

	public getPlayers(message: Message, players) {
		let list: string = __("playerList");
		for (let player of players) {
			list += `${player.name}\n`;
		}
		Global.sendAndReact(message, list, Global.okTag);
	}

	public createPlayer(message: Message) {
		this.getPlayerByIdQuery(message.author.id).exec((err, player) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else if (player) {
				Global.sendAndReact(message, __("playerAlreadyHaveOne", player.name), Global.warnTag);
			} else {
				let splitTmp = message.content.split(" ");
				let name: string = splitTmp.slice(1)[0];
				let description = message.content.substr(splitTmp[0].length + splitTmp[1].length + 2, message.content.length);

				this.getPlayerByNameQuery(name).exec(function (err, player2) {
					if (err) {
						ErrorControlServices.handleError(message, err);
					} else if (player2) {
						Global.sendAndReact(message, __("playerAlreadyExist"), Global.warnTag);
					} else {
						new Player({ idreal: message.author.id, name: name, description: description })
							.save((err) => {
								if (err) {
									ErrorControlServices.handleError(message, err);
								} else {
									message.member.setNickname(name);
									Global.sendAndReact(message, __("playerCreateInfoStat"), Global.okTag);
								}
							});
					}
				});
			}
		});
	}

	protected findRole(message: Message, roleName: string): Role {
		return message.guild.roles.find('name', roleName)
	}

	protected setRole(message: Message, role: Role): void {
		message.member.addRole(role.id);
	}

	public initPlayer(message: Message, player) {
		if (player) {
			let split = message.content.split(" ");
			player.force = parseInt(split[1]);
			player.agilite = parseInt(split[2]);
			player.protection = parseInt(split[3]);
			player.esquive = parseInt(split[4]);
			player.endurance = parseInt(split[5]);
			let somme = player.force + player.agilite + player.protection + player.esquive + player.endurance;
			if (somme === 10) {
				player.save((err) => {
					if (err) {
						ErrorControlServices.handleError(err, message);
					} else {
						this.setRole(message, this.findRole(message, 'nux-345'));
						// TODO : Message dans le mauvais channel !
						Global.sendAndReact(message, __("playerNew", player.name), Global.okTag);
					}
				});
			} else {
				Global.sendAndReact(message, __("playerCreateStatWrongCmd",
					`${split[1]}+${split[2]}+${split[3]}+${split[4]}+${split[5]}`,
					somme, somme), Global.warnTag);
			}
		} else {
			Global.sendAndReact(message, __("playerNotCreate"), Global.warnTag);
		}
	}
}