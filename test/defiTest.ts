import * as Assert from "assert";
import { Message, Role, MessageReaction, User, TextChannel, Guild } from "discord.js";
import { Query } from 'mongoose';
import { __ } from 'i18n';

import TestHelper from "./testHelper";

import Global from "./../src/global";

import AbstractCmd from "./../src/commands/abstractCmd";
import DefiCmd from "./../src/commands/defiCmd";

/*Models*/
import Player from './../src/models/player';

import DefiControlServices from "./../src/control/defiControlServices";
import PlayerControlServices from "./../src/control/playerControlServices";


describe("/defi Cmd", function () {
	this.timeout(5000);

	before((done: MochaDone) => {
		TestHelper.setUpConfig();
		TestHelper.connectToBDD(() => {
			TestHelper.createFixture(done);
		});
	});

	after((done: MochaDone) => {
		TestHelper.deleteFixture(() => {
			TestHelper.disconnectToBDD();
			done();
		});
	});

	it("/defiCreate", (done: MochaDone) => {
		let playerControlServices = new class extends PlayerControlServices {
			public getPlayerByIdQuery(id: string): Query {
				return Player.findOne({ idreal: "1" });
			}
		}

		let defiControlServices = new class extends DefiControlServices {
			sendChannel(message: Message, msg: string): void { }
		}

		let cmd = new DefiCmd(playerControlServices, defiControlServices);

		let message: Message = new class extends Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				return null;
			}
		};
		message.author = new class extends User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Message | Array<Message>)> {
				Assert.equal(content, __("defiMustBeAccept", "def"));
				done();
				return null;
			}
		};
		message.content = "/defiCreate def";

		cmd.handle(message);
	});

	it("/defiAccept", (done: MochaDone) => {
		let playerControlServices = new class extends PlayerControlServices {
			public getPlayerByIdQuery(id: string): Query {
				return Player.findOne({ idreal: "2" });
			}
		}

		let defiControlServices = new class extends DefiControlServices {
			sendChannel(message: Message, msg: string): void { }
		}

		let cmd = new DefiCmd(playerControlServices, defiControlServices);

		let message: Message = new class extends Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				done();
				return null;
			}
		};
		message.author = new class extends User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Message | Array<Message>)> {
				return null;
			}
		};
		message.content = "/defiAccept att";

		cmd.handle(message);
	});
});