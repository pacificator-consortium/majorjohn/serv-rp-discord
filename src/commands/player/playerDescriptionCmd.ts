import { Message } from 'discord.js';

import AbstractCmd from "./../abstractCmd";

/*Models*/
import Player from './../../models/player';

/*Control Services*/
import ErrorControlServices from "./../../control/errorControlServices";
import PlayerControlServices from "./../../control/playerControlServices";


export default class PlayerDescriptionCmd extends AbstractCmd {

	private playerControlServices: PlayerControlServices;

	constructor(playerControlServicesParam: PlayerControlServices) {
		super('/playerDescription', ['/playerDescriptionGet']);
		this.playerControlServices = playerControlServicesParam;
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArg(message, 1);
	}

	public descriptionGet(message: Message) {
		let name = message.content.split(" ").slice(1)[0];
		this.playerControlServices.getPlayerByNameQuery(name).exec((err, player) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				this.playerControlServices.getPlayerDescription(message, player);
			}
		});
	}

	public handle(message: Message) {
		this.descriptionGet(message);
	}
}