import { Message } from "discord.js";

export default class Global {
	public static readonly okTag: string = '✅'; //Check 
	public static readonly warnTag: string = '⚠'; //Warning
	public static readonly errorTag: string = '❗'; //Error
	public static readonly rolesAux: string[] = ['@everyone', 'Admin', 'test-bot'];
	public static lieux = [];

	public static sendWarning(message, msg: string): void {
		message.author.send(msg);
		message.react(Global.warnTag);
	}

	public static sendAndReact(message: Message, msg: string, react: string): void {
		message.author.send(msg);
		message.react(react);
	}
}
