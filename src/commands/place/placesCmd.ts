import { Message } from 'discord.js';
import Global from './../../global';
import { __ } from 'i18n';

import AbstractCmd from "./../abstractCmd";

/*Models*/
import Lieu from './../../models/lieu';

/*Control Services*/
import ErrorControlServices from "./../../control/errorControlServices";
import PlaceControlServices from "./../../control/placeControlServices";


export default class PlayerDescriptionCmd extends AbstractCmd {

	private placeControlServices: PlaceControlServices;

	constructor(placeControlServicesParam: PlaceControlServices) {
		super('/places', ['/places']);
		this.placeControlServices = placeControlServicesParam;
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArg(message, 0);
	}

	public places(message: Message) {
		this.placeControlServices.getPlacesQuery().exec(function (err, lieux) {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				let list: string = __("placeList");
				for (let lieu of lieux) {
					list += `${lieu.name}\n`;
				}
				Global.sendAndReact(message, list, Global.okTag);
			}
		});
	}

	public handle(message: Message) {
		this.places(message);
	}
}