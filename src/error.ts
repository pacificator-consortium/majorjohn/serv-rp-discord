import { getLogger } from 'log4js';
import { __ } from 'i18n';

import Global from './global';

export function error(message, lieu, err): void {
	if (err) {
		message.author.send(__("error"));
		getLogger("error").error(lieu, err);
		getLogger("error").error(`${lieu} InfoSup`, `Cmd: ${message.content} Author: ${message.author.username}/${message.author.id} Channel: ${message.channel.name}`);
		message.react(Global.errorTag);
		throw err;
	}
}

export function errorWithoutMessage(lieu, err): void {
	if (err) {
		getLogger("error").error(lieu, err);
		throw err;
	}
}

