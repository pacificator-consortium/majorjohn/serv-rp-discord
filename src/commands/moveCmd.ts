import { Message, Role } from 'discord.js';
import { getLogger, Logger } from 'log4js';
import { __ } from 'i18n';

import AbstractCmd from "./abstractCmd";

import Global from './../global';

/*Control Services*/
import ErrorControlServices from "./../control/errorControlServices";
import DiscordControlServices from "./../control/discordControlServices";


export default class MoveCmd extends AbstractCmd {

	private logger: Logger;
	private discordControlServices: DiscordControlServices;

	constructor(discordControlServicesParam: DiscordControlServices) {
		super('/move', ['/move']);
		this.discordControlServices = discordControlServicesParam;
		this.logger = getLogger("global");
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArg(message, 1);
	}

	/*	
	*	Destination is a Role in discord.
	*	This methods get the possible destination.
	*/
	// TODO : Move in a control services
	private getAllDestinations(message: Message): string {
		let roles = message.guild.roles.entries();
		let destList: string = "";
		let rolesToIgnore: string[] = this.discordControlServices.getRolesToIgnore();
		let role: Role;
		for (let role of roles) {
			let name: string = role[1].name;
			if (rolesToIgnore.indexOf(name) === -1) {
				destList += `${name}\n`;
			}
		}
		return destList;
	}

	/* Get the role name that correspond to the current */
	// TODO : Move in a control services
	private getPlayerLocation(message: Message): string {
		let rolesDispoIt = message.member.roles.entries();
		let rolesToIgnore: string[] = this.discordControlServices.getRolesToIgnore();
		let role: Role;
		for (let role of rolesDispoIt) {
			let name: string = role[1].name;
			if (rolesToIgnore.indexOf(name) === -1) {
				return name;
			}
		}
	}


	public move(message: Message) {
		let target = message.content.split(" ").slice(1)[0];
		let roleTarget = this.discordControlServices.findRole(message, target);
		//Si la destination existe
		if (roleTarget != null) {
			//recupere le nom du grade
			let source = this.discordControlServices.getChannelName(message).split("_").slice(1)[0];
			if (source === undefined) {
				this.logger.info("move", `Prob Source Introuvable ? Cmd: ${message.content} Author: ${message.author.username}/${message.author.id} Channel: ${this.discordControlServices.getChannelName(message)}`);
				message.react(Global.warnTag);
				Global.sendAndReact(message, __("moveWrongSalon", this.getPlayerLocation(message)), Global.warnTag);
			} else {
				let roleSource = this.discordControlServices.findRole(message, source);
				if (roleSource != null) {
					if (roleSource.name === roleTarget.name) {
						this.logger.info("move", `Deja a cette endroit ! Cmd: ${message.content} Author: ${message.author.username}/${message.author.id} Channel: ${this.discordControlServices.getChannelName(message)}`);
						Global.sendAndReact(message, __("moveAlreadyHere", roleSource.name), Global.warnTag);
					}
					else {
						this.discordControlServices.deleteRole(message, roleSource)
						this.discordControlServices.setRole(message, roleTarget);
						message.react(Global.okTag);
					}
				}
				else {
					this.discordControlServices.setRole(message, roleTarget);
					message.react(Global.okTag);
				}

			}
		}
		else {
			this.logger.info("move", `Prob Dest Introuvable ? Cmd: ${message.content} Author: ${message.author.username}/${message.author.id} Channel: ${this.discordControlServices.getChannelName(message)}`);
			Global.sendAndReact(message, __("moveWrongDestination", this.getAllDestinations(message)), Global.warnTag);
		}
	}



	public handle(message: Message) {
		this.move(message);
	}
}