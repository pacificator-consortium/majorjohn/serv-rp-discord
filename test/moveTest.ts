import * as Assert from "assert";
import { Message, Role, MessageReaction, User, TextChannel, Guild } from "discord.js";
import { __ } from 'i18n';

import TestHelper from "./testHelper";

import Global from "./../src/global";

import AbstractCmd from "./../src/commands/abstractCmd";
import MoveCmd from "./../src/commands/moveCmd";

import DiscordControlServices from "./../src/control/discordControlServices";



describe("/move Cmd", function () {
	this.timeout(5000);

	before((done: MochaDone) => {
		TestHelper.setUpConfig();
		TestHelper.connectToBDD(done);
	});

	after((done: MochaDone) => {
		TestHelper.disconnectToBDD();
		done()
	});

	it("/move", (done: MochaDone) => {
		let discordControlService = new class extends DiscordControlServices {
			findRole(message: Message, roleName: string): Role {
				let role: Role = new Role(new Guild(null, null), {});
				role.name = roleName;
				return role;
			}

			setRole(message: Message, role: Role): void { }

			deleteRole(message: Message, role: Role): void { }

			getRoles(message: Message) { return []; }

			getChannelName(message: Message): string {
				return "plapla_nux-345";
			}
		};

		let cmd = new MoveCmd(discordControlService);

		let message: Message = new class extends Message {
			constructor() {
				super(null, null, null);
			}
			react(emoji): Promise<MessageReaction> {
				Assert.equal(emoji, Global.okTag);
				done();
				return null;
			}
		};
		message.author = new class extends User {
			constructor() {
				super(null, null);
			}
			send(content?, option?): Promise<(Message | Array<Message>)> {
				return null;
			}
		};
		message.content = "/move frigia";

		cmd.handle(message);
	});
});