import * as mongoose from 'mongoose';
import Combat from './../combat';

var defiSchema = new mongoose.Schema({
	att: { type: mongoose.Schema.Types.ObjectId, ref: 'player' },
	def: { type: mongoose.Schema.Types.ObjectId, ref: 'player' },
	winner: String, //att  - def
	combat: String,
	status: Boolean, //True Fini False en Attente
	date: Date
});

defiSchema.methods.accept = function () {
	let diff = ((new Date()).getTime() - (new Date(this.date)).getTime()) / 1000;
	console.log("here")
	if (diff < 43200) { //12h
		let tmp = new Combat(this.att, this.def);
		this.winner = tmp.run();
		this.combat = tmp.getDeroulement();
		this.status = true;
		this.save();
	} else {
		this.remove();
		return "Delais Expiré";
	}
}

defiSchema.methods.refresh = function () {
	this.date = Date.now();
	this.save();
}


export default mongoose.model('defi', defiSchema);

