import { Message } from 'discord.js';

import AbstractCmd from "./../abstractCmd";

/*Models*/
import Player from './../../models/player';

/*Control Services*/
import PlayerControlServices from "./../../control/playerControlServices";


export default class PlayerDescriptionCmd extends AbstractCmd {

	private playerControlServices: PlayerControlServices;

	constructor(playerControlServicesParam: PlayerControlServices) {
		super('/playerCreate', ['/playerCreate']);
		this.locations.push("newplayer");
		this.playerControlServices = playerControlServicesParam;
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArgMin(message, 2);
	}

	public create(message: Message) {
		this.playerControlServices.createPlayer(message);
	}

	public handle(message: Message) {
		this.create(message);
	}
}