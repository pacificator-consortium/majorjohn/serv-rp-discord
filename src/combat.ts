import Player from './models/player';

export default class Combat {

	private j1;
	private j2;
	private deroulement: string;

	/*
	* [Att: name][Def: name]
	*/
	/*
	* 1 - Tete
	* 2 - Parade
	* 3 - No Effect
	*/

	constructor(j1, j2) {
		console.log("Start")
		this.j1 = j1;
		console.log(`j1: ${this.j1}`);
		this.j2 = j2;
		this.deroulement = "";
	}

	private roll(): number {
		let res = this.rand(6)
		if (1 <= res && res <= 3) {
			return 1;
		} else if (res === 4) {
			return 3;
		} else {
			return 2;
		}
	}

	private rollType(nb, type): number {
		let cpt = 0;
		for (let i = 0; i < nb; ++i) {
			let des = this.roll();
			if (type == des) {
				cpt++;
			}
		}
		return cpt;
	}

	private rand(max): number {
		return Math.floor((Math.random() * max) + 1);
	}


	private esquive(j1, j2): boolean {
		let tauxEsquive = 5;
		if (j2.esquive - j1.agilite > 0) {
			tauxEsquive += (j2.esquive - j1.agilite) * 16
		}
		//Esquive si rand < TauxEsquive
		return (this.rand(100) <= tauxEsquive);
	}

	/*J1 Attaque J2*/
	private attaque(j1, j2): number {
		this.deroulement += `[Attaquant ${j1.name}]: `
		/*J1 Start*/
		let degat = 0;
		if (!this.esquive(j1, j2)) {
			degat = this.rollType(j1.force, 1) - this.rollType(j2.protection, 2);
			if (degat < 0) {
				degat = 0;
			}
			this.deroulement += `${j2.name} prends ${degat} !}`;
		} else {
			this.deroulement += `${j2.name} esquive l'attaque !}`;
		}
		return degat;
	}

	public getDeroulement(): string {
		return this.deroulement;
	}

	public run(): string {
		this.j1.endurance *= 10;
		this.j2.endurance *= 10;
		if (this.j1.endurance === 0) {
			this.j1.endurance = 1;
		}
		if (this.j2.endurance === 0) {
			this.j2.endurance = 1;
		}
		this.deroulement += `[Att: ${this.j1.name} HP: ${this.j1.endurance}][Def: ${this.j2.name} HP: ${this.j2.endurance}]\n`;
		let turn = 1;
		let nbTurn = 0;
		while (this.j2.endurance > 0 && this.j1.endurance > 0 && nbTurn < 500) {
			this.deroulement += `[Turn: ${nbTurn + 1}]`;
			if (turn === 1) {
				this.j2.endurance -= this.attaque(this.j1, this.j2);
				this.deroulement += ` Il lui reste ${this.j2.endurance} HP`
				turn++;
			} else {
				this.j1.endurance -= this.attaque(this.j2, this.j1);
				this.deroulement += ` Il lui reste ${this.j1.endurance} HP`
				turn = 1;
			}
			nbTurn++;
			this.deroulement += `\n`;
		}
		if (nbTurn == 500) {
			this.deroulement += `[Match Nul]`;
			return "Match Nul";
		} else if (this.j1.endurance === 0) {
			this.deroulement += `[${this.j2.name} à Gagné !]`;
			return "${this.j2.name} à Gagné !";
		} else if (this.j2.endurance === 0) {
			this.deroulement += `[${this.j1.name} à Gagné !]`;
			return "${this.j1.name} à Gagné !";
		} else {
			this.deroulement += `[WUT ? Une erreur est survenu !]`;
			return "err";
		}
	}
}
