import { getLogger, Logger } from 'log4js';
import { Message } from 'discord.js';

import Global from '../global';

export default class PermissionsControlServices {
	private static logger = getLogger("error");

	private constructor() { }

	public static handleError(message: Message, err): void {
		if (err) {
			PermissionsControlServices.logger.error(err);
		} else {
			PermissionsControlServices.logger.error("Object was Null: " + message.content);
		}
		message.react(Global.errorTag);
	}
}