import Global from './../global';
import { error } from './../error';
import { __ } from 'i18n';

/*KeepIT*/
import { Query } from 'mongoose';
import { Message, TextChannel, Role } from 'discord.js';
import { Logger, getLogger } from 'log4js';




export default class DiscordControlServices {
	private static rolesToIgnore: string[] = ['@everyone', 'Admin', 'test-bot'];

	public findRole(message: Message, roleName: string): Role {
		return message.guild.roles.find('name', roleName)
	}

	public setRole(message: Message, role: Role): void {
		message.member.addRole(role.id);
	}

	public deleteRole(message: Message, role: Role): void {
		message.member.removeRole(role.id);
	}

	public getRoles(message: Message) {
		return message.guild.roles.entries();
	}

	public getRolesToIgnore(): string[] {
		return DiscordControlServices.rolesToIgnore;
	}

	public getChannelName(message: Message): string {
		return (<TextChannel>message.channel).name;
	}
}