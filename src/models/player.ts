import { Document, Schema, model } from 'mongoose';


export interface IPlayer extends Document {
	idreal: string;
	name: string;
	description: string;
	force : number;
	agilite : number;
	protection : number;
	esquive : number;
	endurance : number;
	location : string;
}

var playerSchema = new Schema({
	idreal : String,
	name : String,
	description : String,
	force : { type: Number, max: 10, min: 0 },
	agilite : { type: Number, max: 10, min: 0 },
	protection : { type: Number, max: 10, min: 0 },
	esquive : { type: Number, max: 10, min: 0 },
	endurance : { type: Number, max: 10, min: 0 },
	location : { type: String, default: null}
});

export default model<IPlayer>('player', playerSchema);