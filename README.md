# Code source du bot DraftRP (Toffee)

Requière: mongodb, shell, node

Pour l'installation du projet :
```
npm install
//Set Up du dossier config [config/token.cfg]
```

Pour la compilation :
```
npm run build
```

Pour lancer les tests :
```
npm test
```

Pour l'excution :
```
//Requière mongod de démarré
npm run run
```

## Installation avec Docker

commit du package-lock.json pour ne pas avoir des problèmes de retro compatibilité sur les librairies. Etat récuperer de la compilation du serveur datant du 25/01/2020

Pour la compilation :
```
docker-compose build
```

Pour l'execution :
```
docker-compose up
```

Pour l'execution sur le server :
```
docker-compose up --detach
```

Pour stop l'excution sur le server :
```
docker-compose down
```


Back up mongodb
```
docker container ls -a // Liste tout les container

docker exec -it MONGO_CONTAINER sh
mkdir backup
cd backup
mongodump -h localhost:27017 -d servrp -o .

// Sortir du container
docker cp MONGO_CONTAINER:/backup .
```

# Les fichiers de config :
Le token.cfg contien uniquement le token en brut du bot trouvable sur https://discordapp.com/developers/applications/me

# Ajouter un bot :
Pour ajouter un bot à un salon il faut le client_id du bot ainsi que générer un chiffre correspondant au permission voulu. Des outils sont disponible coté outils developper de discord
https://discordapp.com/oauth2/authorize?client_id=[CLIENT_ID]&scope=bot&permissions=[PERMISSIONS]

# User de test:
email: draftcorporationfr@gmail.com

# To Do List :  

# [0.2.0]
- [X] Refacto environnement 
- [X] Stabilisation
- [ ] Verification des commandes
	- [X] /playerCreate
	- [X] /playerDescriptionGet
	- [X] /players
	- [X] /playerStatInit
	- [X] /defiCreate
	- [ ] /defiAccept
		- [ ] KO lors du combat.
	- [X] /move
- [ ] Système de lieu
	- [ ] /places -> KO


# [X.X.X]
- [X] MaJ des versions des dépendances
- [X] token dans un fichier appart
- [X] Intégration de log4js
- [X] Indentation
- [X] Intégration de i18n-node pour l'extraction des strings (Premier pas vers l'internationnalisation)
- [ ] Refactor Global
	- [ ] Classe global de Cmd
	- [ ] Découpage en controlService
- [ ] Fonctionnalité des Lieux
- [ ] Branchement des combats
- [ ] Cmd Test


# Problème connu :
- [ ] Priviliege too low
- [ ] Promise Warning
- [ ] La commande /playerStatInit envoie le message de bienvenu en privé.
- [ ] Si un joueur lance un défi alors qu'il n'a pas de stat -> Crash
- [ ] Si un joueur lance deux défi à un même joueur -> Crash
- [ ] Le message de victoire est non valide : ${this.j2.name} à Gagné !