import { Message, Role } from 'discord.js';
import { getLogger, Logger } from 'log4js';
import { __ } from 'i18n';

import AbstractCmd from "./abstractCmd";

import Global from './../global';

/*Control Services*/
import PlayerControlServices from "./../control/playerControlServices";
import DefiControlServices from "./../control/defiControlServices";
import ErrorControlServices from "./../control/errorControlServices";


export default class MoveCmd extends AbstractCmd {

	private logger: Logger;
	private playerControlServices: PlayerControlServices;
	private defiControlServices: DefiControlServices;

	constructor(playerControlServicesParam: PlayerControlServices,
		defiControlServicesParam: DefiControlServices) {
		super('/defi', ['/defiCreate', '/defiAccept']);
		this.playerControlServices = playerControlServicesParam;
		this.defiControlServices = defiControlServicesParam;
		this.logger = getLogger("global");
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArg(message, 1);
	}

	public create(message: Message) {
		let name = message.content.split(" ").slice(1)[0];
		//Get J Courant
		this.playerControlServices.getPlayerByIdQuery(message.author.id).exec((err, att) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				if (att) {
					//Get Def
					this.playerControlServices.getPlayerByNameQuery(name).exec((err, def) => {
						if (err) {
							ErrorControlServices.handleError(message, err);
						} else {
							//Check Already Defi In wait
							if (def) {
								this.defiControlServices.createDefi(message, att, def);
							} else {
								Global.sendAndReact(message, __("playerUnknow"), Global.warnTag);
							}
						}
					});
				} else {
					Global.sendAndReact(message, __("playerNotCreate"), Global.warnTag);
				}
			}
		});
	}

	public accept(message: Message) {
		let name = message.content.split(" ").slice(1)[0];
		this.playerControlServices.getPlayerByIdQuery(message.author.id).exec((err, def) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				if (def) {
					this.defiControlServices.acceptDefi(message, def);
				} else {
					Global.sendAndReact(message, __("playerNotCreate"), Global.warnTag);
				}
			}
		});
	}

	public handle(message: Message) {
		if (this.getCmd(message.content) === this.getValidCmds()[0]) {
			this.create(message);
		} else {
			this.accept(message);
		}
	}
}