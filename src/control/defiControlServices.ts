import Global from './../global';
import { error } from './../error';
import { __ } from 'i18n';

/*KeepIT*/

import { Query } from 'mongoose';
import { Message, TextChannel, Role } from 'discord.js';
import { Logger, getLogger } from 'log4js';

/*Models*/
import Defi from './../models/defi';

/*Control Services*/
import ErrorControlServices from "./errorControlServices";


export default class DefiControlServices {
	private static logger: Logger = getLogger("error");

	public getDefi(att, def) {
		return Defi.findOne({ att: att, def: def, status: false });
	}

	public getDefiByDef(def) {
		return Defi.findOne({ def: def._id, status: false }).populate('att').populate('def');
	}

	sendChannel(message: Message, msg: string): void {
		message.channel.send(msg);
	}

	public createDefi(message: Message, att, def): void {
		this.getDefi(att, def).exec((err, defi: any) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				if (defi) {
					defi.refresh();
					Global.sendAndReact(message, __("defiWait", name), Global.okTag);
				} else {
					new Defi({ att: att, def: def, winner: "", combat: "", status: false, date: Date.now() })
						.save((err) => {
							if (err) {
								ErrorControlServices.handleError(message, err);
							} else {
								this.sendChannel(message, __("defiLaunch", att.name, def.name));
								Global.sendAndReact(message, __("defiMustBeAccept", def.name), Global.okTag);
							}
						});
				}
			}
		});
	}

	public acceptDefi(message: Message, def): void {
		this.getDefiByDef(def).exec((err, defi: any) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				this.sendChannel(message, __("defiAccept", def.name, defi.att.name))
				defi.accept();
				this.sendChannel(message, defi.winner);
				message.react(Global.okTag);
			}
		});
	}
}