import { Message } from 'discord.js';

import AbstractCmd from "./../abstractCmd";

/*Models*/
import Player from './../../models/player';

/*Control Services*/
import ErrorControlServices from "./../../control/errorControlServices";
import PlayerControlServices from "./../../control/playerControlServices";


export default class PlayerStatCmd extends AbstractCmd {

	private playerControlServices: PlayerControlServices;

	constructor(playerControlServicesParam: PlayerControlServices) {
		super('/playerStat', ['/playerStatInit']);
		this.playerControlServices = playerControlServicesParam;
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArg(message, 5);
	}

	public statInit(message: Message) {
		this.playerControlServices.getPlayerByIdQuery(message.author.id).exec((err, player) => {
			if (err) {
				ErrorControlServices.handleError(message, err);
			} else {
				this.playerControlServices.initPlayer(message, player);
			}
		});
	}

	public handle(message: Message) {
		this.statInit(message);
	}
}