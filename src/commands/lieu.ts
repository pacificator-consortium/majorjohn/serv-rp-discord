import { getLogger, Logger } from 'log4js';
import { __ } from 'i18n';

import Global from './../global';
import { error, errorWithoutMessage } from './../error';

import LieuModel from './../models/lieu';
import PlayerModel from './../models/player';

export default class Lieu {
	private command = '/lieu';
	private logger: Logger;

	constructor() {
		this.logger = getLogger("global");
	}

	public getLieux(message) {
		LieuModel.find({}).exec(function (err, lieux) {
			error(message, "getLieux", err);
			let list = __("placeList");
			for (let lieu of lieux) {
				list += `${lieu.name}\n`;
			}
			message.author.send(list);
			message.react(Global.okTag);
		});
	}

	public static loadLieux() {
		LieuModel.find({}).exec(function (err, lieux) {
			getLogger("global").info("lieu.ts", "Load All Lieux");
			errorWithoutMessage("lieu.ts", err);
			for (let lieu of lieux) {
				if (lieu.checkValid()) {
					Global.lieux.push(lieu);
					console.log(lieu.name);
				}
			}
			getLogger("global").info("lieu.ts", `${Global.lieux.length} Lieux Load.`);
		});
	}


	/* Handle Lieux in classic Text*/
	public static handleLieu(message) {
		PlayerModel.findOne({ idreal: message.author.id }).exec(function (err, player) {
			error(message, "HandleLieu", err);
			if (player) {
				/*Location*/
				if (player.location != null && message.member.nickname != `[${player.location}] ${player.name}`)
					message.member.setNickname(`[${player.location}] ${player.name}`)
						.catch(err => this.logger.info("lieu.ts", `${err}`));
				/*Ref*/
				let match = message.content.match(new RegExp(/#(\w(\s?))*\w#/, "gi"));
				console.log(match);
				if (match) {
					match.forEach((fullname) => {
						let isIn = false;
						let name = fullname.substr(1, fullname.length - 2);
						//Prb
						let index = -1
						for (let i = 0; i < Global.lieux.length; ++i) {
							if (Global.lieux[i].name === name) {
								index = i;
								break;
							}
						}
						if (index != -1) {
							//Deplacement + Frequentation
							Global.lieux[index].freq(player);
						} else {
							let lieu = new LieuModel({ name: name, creator: player, frequetationNet: [], reputationNet: [], participant: [] });
							lieu.save(function (err) { error(message, "createLieu", err); });
							Global.lieux.push(lieu);
							player.location = lieu.name;
							player.save();
							//Deplacement
						}

						if (message.member.nickname != `[${player.location}] ${player.name}`)
							message.member.setNickname(`[${player.location}] ${player.name}`)
					});
				}
				Global.lieux.forEach((lieu) => {
					let index = message.content.indexOf(lieu.name);
					if (index != -1 && message.content.indexOf(`#${lieu.name}#`) == -1) {
						if (!(/\w/.test(message.content.charAt(index + lieu.name.length)))) {
							lieu.rep(player);
						}
					}
				});
			} else {
				Global.sendWarning(message, __("playerNotCreate"));
			}
		});
	}

	public getLieuDescription(message) {
		let index = message.content.indexOf(" ");
		//Si pas d'espace index == -1 et osef.
		let name = message.content.substr(index + 1);
		LieuModel.findOne({ name: name }).populate("creator").populate("participant").exec(function (err, lieu) {
			error(message, "getLieuDescription", err);
			if (lieu) {
				let res = __("placeDescription", lieu.name, lieu.description, lieu.creator.name);
				if (lieu.participant.length > 0)
					res += __("placeDescriptionContribute");
				lieu.participant.forEach((p) => res += ` ${p.name}`)
				message.author.send(res);
				message.react(Global.okTag);
			} else {
				Global.sendWarning(message, __("placeUnknow"));
			}
		});
	}

	public handle(message) {
		/*Slice récupère des bout d'un tableau*/
		let cmd = message.content.split(" ")[0];

		if (cmd == "/lieux")
			this.getLieux(message);
		else if (cmd == "/lieuGetDescription")
			this.getLieuDescription(message);
		else
			Global.sendWarning(message, __("cmdUnknow"));
	}
}