import { Document, Schema, Model, model } from 'mongoose';

import {IPlayer} from './player';

export interface IPlace extends Document {
	frequentationBrut: number;
	frequentationNet: IPlayer[];
	reputationBrut: number;
	reputationNet: IPlayer[];
	name: string;
	description: string;
	creation: Date;
	creator: IPlayer;
	participant: IPlayer[];
	valid: boolean;

	checkValid: () => boolean;
	freq: (player: IPlayer) => void;
	rep: (player) => string;
}

var lieuSchema = new Schema({
	frequentationBrut: { type: Number, default: 0 },
	frequentationNet: [{ type: Schema.Types.ObjectId, ref: 'player' }],
	reputationBrut: { type: Number, default: 0 },
	reputationNet: [{ type: Schema.Types.ObjectId, ref: 'player' }],
	name: String,
	description: { type: String, default: "" },
	creation: { type: Date, default: Date.now()},
	creator: { type: Schema.Types.ObjectId, ref: 'player' },
	participant: [{ type: Schema.Types.ObjectId, ref: 'player' }],
	valid: { type: Boolean, default: false }
});

lieuSchema.methods.checkValid = function() {
	if(!this.valid){
		let diff = ((new Date()).getTime() - (new Date(this.date)).getTime()) / 1000;
		if(diff > 604800 && this.frequentationNet.length < 2){ //7j
			this.remove();
			return false;
		} 
	}
	return true;
}

// TODO : Move in control Service
lieuSchema.methods.freq = function(player: IPlayer) {
	if(this.frequentationNet.indexOf(player) == -1){
		this.frequentationNet.push(player);
	}
	if (this.frequentationNet.length >= 2) {
		this.valid = true;
	}
	this.frequentationBrut+=1;
	player.location = this.name;
	player.save();
	this.save();
}

// TODO : Move in control Service
lieuSchema.methods.rep = function(player) {
	if(this.reputationNet.indexOf(player._id) == -1){
		this.reputationNet.push(player);
	}
	this.reputationBrut+=1;
	this.save();
}

export default model<IPlace>('lieu', lieuSchema);

