import { Message } from 'discord.js';

import AbstractCmd from "./../abstractCmd";

/*Models*/
import Player from './../../models/player';

/*Control Services*/
import ErrorControlServices from "./../../control/errorControlServices";
import PlayerControlServices from "./../../control/playerControlServices";


export default class PlayersCmd extends AbstractCmd {

	private playerControlServices: PlayerControlServices;

	constructor(playerControlServicesParam: PlayerControlServices) {
		super('/players', ['/players']);
		this.playerControlServices = playerControlServicesParam;
	}

	public verifyArgValid(message: string): boolean {
		return this.checkArg(message, 0);
	}

	public players(message: Message) {
		this.playerControlServices.getPlayersQuery().exec((err, players) => {
			if (err || !players) {
				ErrorControlServices.handleError(message, err);
			} else {
				this.playerControlServices.getPlayers(message, players);
			}
		});
	}

	public handle(message: Message) {
		this.players(message);
	}
}